#ifndef DATA_COLLECTION_INS_UTILS_H
#define DATA_COLLECTION_INS_UTILS_H


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/serial.h>  
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;

#define PI 3.145926
#define DATASIZE 55

int UART0Open(int &fd, char* port);
void UART0Close(int fd);
int UART0Set(int fd, int speed, int flow_ctrl, int databits, int stopbits, int parity);
int UART0Init(int fd, int speed, int flow_ctrl, int databits, int stopbits, int parity);
int UART0Recv(int fd, unsigned char *rcv_buf, double *xyangles, int data_len);

void DecodeIMUData(unsigned char chrTemp[]);
int read_wait(int fd, unsigned char *rcv_buf, double *xyangles, int data_len);


#endif

