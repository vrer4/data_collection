#include <ros/ros.h>
#include <libserial/SerialPort.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <unistd.h>

#include <data_collection/serial_msg.h>

using namespace LibSerial;


class SerialPublisher {
public:
    SerialPublisher(std::string dev) : serial_dev(dev) {
        serial_pub_  = n_.advertise<data_collection::serial_msg>("serial_data", 10);
    }

    void publish() {

        try{
            serial_port.Open(serial_dev.c_str()) ;
        }
        catch (const OpenFailed&){
            ROS_INFO("The serial port did not open correctly. (%s)" ,serial_dev.c_str());
            return  ;
        }

        ROS_INFO("Device Serial Port is Opening....");

        serial_port.SetBaudRate(BaudRate::BAUD_9600) ;
        serial_port.SetCharacterSize(CharacterSize::CHAR_SIZE_8) ;
        serial_port.SetFlowControl(FlowControl::FLOW_CONTROL_NONE) ;
        serial_port.SetParity(Parity::PARITY_NONE) ;
        serial_port.SetStopBits(StopBits::STOP_BITS_1) ;

        while(!serial_port.IsDataAvailable()) {
            usleep(1000) ;
        }


        // Specify a timeout value (in milliseconds).
        size_t ms_timeout = 250 ;

        data_collection::serial_msg msg;
        
        while(1) {
            try{
                // Read as many bytes as are available during the timeout period.
                serial_port.Read(read_buffer, 0, ms_timeout) ;
            }
            catch (const ReadTimeout&){
                for (size_t i = 0 ; i < read_buffer.size() ; i++) {
                    //std::cout << read_buffer.at(i) << std::flush ;
                    msg.data.emplace_back(read_buffer.at(i));
                }
                if(read_buffer.size()) {
                    serial_pub_.publish(msg);
                    msg.data.clear();
                }

            }
        }

    }

private:
    ros::NodeHandle n_;
    ros::Publisher serial_pub_;

    SerialPort serial_port;
    std::string serial_dev;
    DataBuffer read_buffer;
};



int main(int argc, char** argv) {

    ros::init(argc, argv, "serial_pub_node");
    SerialPublisher serialPublisher("/dev/microhard");
    serialPublisher.publish();
    ros::spin();
    return 0;
}