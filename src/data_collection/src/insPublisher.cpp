#include <ros/ros.h>

#include <data_collection/ins_msg.h>
#include <data_collection/ins_utils.h>


class INSPublisher {

public:
    INSPublisher() {
        ins_pub = n_.advertise<data_collection::ins_msg>("ins_data", 10);
    }

    void publish() {
        data_collection::ins_msg msg;
        std::string s;
        n_.getParam("ins_dev", s);
        UART0Open(fd, (char*) s.c_str());
        if(fd == -1)
        {
            ROS_INFO("Can't open serial port(%s):", s.c_str());
            return ;
        }
        ROS_INFO("Device INS is Opening....");
        if(UART0Init(fd, 115200, 0, 8, 1, 'N') == -1)
        {
            printf("Set port failed\n");
            return;
        }
        while(1)
        {
            try
            {
                if(UART0Recv(fd, buffer, xyangle, DATASIZE) == -1)
                {
                    printf("Can't read data from INS\n");  
                    break;
                }
                msg.x = xyangle[0];
                msg.y = xyangle[1];
                //msg.data = buffer;
                ins_pub.publish(msg);
            }catch(exception e){
                continue;
            }
        }
    }


private:
    ros::NodeHandle n_;
    ros::Publisher ins_pub;
    int fd, off = 1;
    unsigned char *p = NULL;
    unsigned char buffer[DATASIZE];
    double xyangle[2];

};

int main(int argc, char** argv) {

    ros::init(argc, argv, "ins_pub_node");
    INSPublisher insPublisher;
    insPublisher.publish();
    ros::spin();
    return 0;
}