#include "data_collection/ins_utils.h"

double a[3], w[3], Angle[3], myAngle[3], T;
int i = 0;
vector<double> a_avg = {0, 0, 0};
struct timeval _start_, _end_;
long start = 0, end_ = 0;
int MODE_ = -1, iterSize = 5;
double x, y;

int UART0Open(int &fd, char* port){
    fd = open(port, O_RDONLY|O_NOCTTY|O_NDELAY);  
	if (-1 == fd)  
	{  
		perror("Can't Open Serial Port");  
		return -1;  
	}  
	//恢复串口为阻塞状态                                 
	if(fcntl(fd, F_SETFL, 0) < 0)  
	{  
		printf("fcntl failed!\n");
		return -1;  
	}       
	else  
	{  
		printf("fcntl=%d\n",fcntl(fd, F_SETFL,0));  
	}  
	//测试是否为终端设备      
	if(0 == isatty(STDIN_FILENO))  
	{  
		printf("standard input is not a terminal device\n");  
		return -1;  
	}  
	else  
	{  
		printf("isatty success!\n");  
	}                
	printf("fd->open=%d\n",fd);  
	return fd;  
}

void UART0Close(int fd)
{
    close(fd);
}

int UART0Set(int fd, int speed, int flow_ctrl,int databits,int stopbits,int parity)
{
    int   i;  
	int   status;  
	int   speed_arr[] = { B115200, B19200, B9600, B4800, B2400, B1200, B300};  
	int   name_arr[] = {115200,  19200,  9600,  4800,  2400,  1200,  300};  
           
	struct termios options;  
     
	/*tcgetattr(fd,&options)得到与fd指向对象的相关参数，并将它们保存于options,该函数还可以测试配置是否正确，该串口是否可用等。若调用成功，函数返回值为0，若调用失败，函数返回值为1. 
    */  
	if( tcgetattr( fd,&options)  !=  0)  
	{  
		perror("SetupSerial 1");      
		return -1;   
	}  
    
    //设置串口输入波特率和输出波特率  
	for ( i= 0;  i < sizeof(speed_arr) / sizeof(int);  i++)  
	{  
		if  (speed == name_arr[i])  
		{               
			cfsetispeed(&options, speed_arr[i]);   
			//cfsetospeed(&options, speed_arr[i]);    
		}  
	}      
    struct serial_struct serial; 
    //修改控制模式，保证程序不会占用串口  
    options.c_cflag |= CLOCAL;  
    //修改控制模式，使得能够从串口中读取输入数据  
    options.c_cflag |= CREAD;  
    ioctl(fd, TIOCGSERIAL, &serial); 
    serial.flags &= ~ASYNC_LOW_LATENCY; 
    serial.xmit_fifo_size = 1024; // what is "xmit" ?? 
    ioctl(fd, TIOCSSERIAL, &serial);
    //设置数据流控制  
    switch(flow_ctrl)  
    {  
        
		case 0 ://不使用流控制  
              options.c_cflag &= ~CRTSCTS;  
              break;     
        
		case 1 ://使用硬件流控制  
              options.c_cflag |= CRTSCTS;  
              break;  
		case 2 ://使用软件流控制  
              options.c_cflag |= IXON | IXOFF | IXANY;  
              break;  
    }  
    //设置数据位  
    //屏蔽其他标志位  
    options.c_cflag &= ~CSIZE;  
    switch (databits)  
    {    
		case 5    :  
                     options.c_cflag |= CS5;  
                     break;  
		case 6    :  
                     options.c_cflag |= CS6;  
                     break;  
		case 7    :      
                 options.c_cflag |= CS7;  
                 break;  
		case 8:      
                 options.c_cflag |= CS8;  
                 break;    
		default:     
                 fprintf(stderr,"Unsupported data size\n");  
                 return -1;   
    }  
    //设置校验位  
    switch (parity)  
    {    
		case 'n':  
		case 'N': //无奇偶校验位。  
                 options.c_cflag &= ~PARENB;   
                 options.c_iflag &= ~INPCK;      
                 break;   
		case 'o':    
		case 'O'://设置为奇校验      
                 options.c_cflag |= (PARODD | PARENB);   
                 options.c_iflag |= INPCK;               
                 break;   
		case 'e':   
		case 'E'://设置为偶校验    
                 options.c_cflag |= PARENB;         
                 options.c_cflag &= ~PARODD;         
                 options.c_iflag |= INPCK;        
                 break;  
		case 's':  
		case 'S': //设置为空格   
                 options.c_cflag &= ~PARENB;  
                 options.c_cflag &= ~CSTOPB;  
                 break;   
        default:    
                 fprintf(stderr,"Unsupported parity\n");      
                 return -1;   
    }   
    // 设置停止位   
    switch (stopbits)  
    {    
		case 1:     
                 options.c_cflag &= ~CSTOPB; break;   
		case 2:      
                 options.c_cflag |= CSTOPB; break;  
		default:     
                       fprintf(stderr,"Unsupported stop bits\n");   
                       return -1;  
    }  
    //ioctl(fd,)
	//修改输出模式，原始数据输出  
	options.c_oflag &= ~OPOST;  
    
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);  
	//options.c_lflag &= ~(ISIG | ICANON);  
     
    //设置等待时间和最小接收字符  
    options.c_cc[VTIME] = 0; /* 读取一个字符等待1*(1/10)s */    
    options.c_cc[VMIN] = 1; /* 读取字符的最少个数为1 */  
     
    //如果发生数据溢出，接收数据，但是不再读取 刷新收到的数据但是不读  
    tcflush(fd,TCIFLUSH);  
     
    //激活配置 (将修改后的termios数据设置到串口中）  
    if (tcsetattr(fd,TCSANOW,&options) != 0)    
	{  
		perror("com set error!\n");    
		return -1;   
	}  
    return 0;   
}

int UART0Init(int fd, int speed, int flow_ctrl, int databits, int stopbits, int parity)
{
    int err;
    //设置串口数据帧模式
    if(UART0Set(fd, speed, flow_ctrl, databits, stopbits, parity) == -1)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

void DecodeIMUData(unsigned char chrTemp[], double xyangles[])
{
	switch (chrTemp[1])
	{
	case 0x51:
		a[0] = (short(chrTemp[3] << 8 | chrTemp[2])) / 32768.0 * 16;
		a[1] = (short(chrTemp[5] << 8 | chrTemp[4])) / 32768.0 * 16;
		a[2] = (short(chrTemp[7] << 8 | chrTemp[6])) / 32768.0 * 16;
		T = (short(chrTemp[9] << 8 | chrTemp[8])) / 340.0 + 36.25;
		if (i == iterSize) {
			for (int j = 0; j < 3; j++)
				a_avg[j] /= iterSize;
			myAngle[0] = atanf(a_avg[1] / (a_avg[2])) / PI * 180;
			myAngle[1] = -atanf((a_avg[0]) / (a_avg[2])) / PI * 180; 
            gettimeofday(&_end_, NULL);
			end_ = ((long)_end_.tv_sec)*1000 + (long)_end_.tv_usec/1000;
			//printf("\rmyAngel = x轴：%4.2f°\t y轴%4.2f°\tz轴a：%4.2f\tx轴a：%4.2f\ty轴a：%4.2f\t", myAngle[0], myAngle[1], a_avg[2] - 0.02, a_avg[0] - 0.05, a_avg[1]);
			//printf("myAngel = x轴：%4.2f°\t y轴%4.2f°\n", myAngle[0], myAngle[1]);
            xyangles[0] = myAngle[0];
            xyangles[1] = myAngle[1];
			//printf("z轴原始a：%4.2f z轴计算a：%4.2f\n", a_avg[2], a[2]);
			//cout << "time:" << end_ - start << "ms" << endl;
			for (int j = 0; j < 3; j++)
				a_avg[j] = 0;
			i = 0;
		}
		else {
			if (i == 0)
            {
                gettimeofday(&_start_, NULL);
				start = ((long)_start_.tv_sec)*1000 + (long)_start_.tv_usec/1000;
            }
			for (int j = 0; j < 3; j++)
				a_avg[j] += a[j];
			i++;
		}
		break;
	case 0x52:
		w[0] = (short(chrTemp[3] << 8 | chrTemp[2])) / 32768.0 * 2000;
		w[1] = (short(chrTemp[5] << 8 | chrTemp[4])) / 32768.0 * 2000;
		w[2] = (short(chrTemp[7] << 8 | chrTemp[6])) / 32768.0 * 2000;
		T = (short(chrTemp[9] << 8 | chrTemp[8])) / 340.0 + 36.25;
		break;
	case 0x53:
		Angle[0] = (short(chrTemp[3] << 8 | chrTemp[2])) / 32768.0 * 180;
		Angle[1] = (short(chrTemp[5] << 8 | chrTemp[4])) / 32768.0 * 180;
		Angle[2] = (short(chrTemp[7] << 8 | chrTemp[6])) / 32768.0 * 180;
		T = (short(chrTemp[9] << 8 | chrTemp[8])) / 340.0 + 36.25;
		break;
	}
}

int read_wait(int fd, unsigned char *rcv_buf, int data_len)
{
    int off = 0;
    while(off < data_len){
        int p = read(fd, rcv_buf + off, data_len - off);
        if(p > 0)
            off += p;
        else
        {
            return p;
        }
        
    }
    return off;
}

int UART0Recv(int fd, unsigned char *rcv_buf, double *xyangles, int data_len)  
{  
    int off = 1;
    unsigned char *p = rcv_buf - 1;
	int len;  
    len = read_wait(fd, rcv_buf, data_len);    
    do{
         p = find(p + 1, rcv_buf + DATASIZE, 0x55);
    }while (p != rcv_buf + DATASIZE && p[1] != 0x51) ;
    off = p - rcv_buf;
    memmove(rcv_buf, p, DATASIZE - off);
    read_wait(fd, rcv_buf + DATASIZE - off, off);
    // for(int i=0; i < DATASIZE; i++)
    //     printf("%02X ",rcv_buf[i]);
    // printf("\n\n");
    if(rcv_buf[0] == 0x55 && rcv_buf[1] == 0x51 && rcv_buf[DATASIZE-1] == 0xac)
        for (int i = 0; i < 5; i++)
            DecodeIMUData(rcv_buf + i * 11, xyangles);
    return len;
}
