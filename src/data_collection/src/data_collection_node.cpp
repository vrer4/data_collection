#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>

#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>

#include <data_collection/ins_msg.h>
#include <data_collection/serial_msg.h>

class DataCollection {
public:
    DataCollection() {
        ROS_INFO("Data is collecting which is from Lidar, camera, ins, serial port");
        point16_sub = n_.subscribe<sensor_msgs::PointCloud2>("velodyne16/velodyne_points", 100, &DataCollection::point16CallBack, this);
        point32_sub = n_.subscribe<sensor_msgs::PointCloud2>("velodyne32/velodyne_points", 100, &DataCollection::point32CallBack, this);
        //lidar_sub = n_.subscribe<sensor_msgs::LaserScan>("scan", 10, &DataCollection::lidarCallBack, this);
        pic_sub = n_.subscribe<sensor_msgs::Image>("usb_cam/image_raw", 10, &DataCollection::picCallBack, this);
        ins_sub = n_.subscribe<data_collection::ins_msg>("ins_data", 10, &DataCollection::insCallBack, this);
        serial_sub = n_.subscribe<data_collection::serial_msg>("serial_data", 10, &DataCollection::serialCallBack, this);
    }

private:
    void point16CallBack(const sensor_msgs::PointCloud2::ConstPtr& pc) {
        //ROS_INFO("Height: %d Width: %d ", pc->height, pc->width);
        if(primary_idx == 0 || pc->height == 0 || pc->width == 0) return;
        pcl::PointCloud<pcl::PointXYZ> cloud;
        pcl::fromROSMsg(*pc, cloud);
        pcl::io::savePCDFileASCII(data_path + "/pointcloud16/" + std::to_string(primary_idx) + "_" + 
                        std::to_string(point16_idx++)+".pcd", cloud);
    }

    void point32CallBack(const sensor_msgs::PointCloud2::ConstPtr& pc) {
        //ROS_INFO("Height: %d Width: %d ", pc->height, pc->width);
        if(pc->height == 0 || pc->width == 0) return;
        pcl::PointCloud<pcl::PointXYZ> cloud;
        pcl::fromROSMsg(*pc, cloud);
        pcl::io::savePCDFileASCII(data_path + "/pointcloud32/" + std::to_string(primary_idx++)+".pcd", cloud);
    }

    void lidarCallBack(const sensor_msgs::LaserScan::ConstPtr& pc) {
        ROS_INFO("Lidar Call Back");
    }

    void picCallBack(const sensor_msgs::Image::ConstPtr& pic) {
        if(primary_idx == 0 || pic->height == 0 || pic->width == 0) return;
        cv_bridge::CvImageConstPtr cv_ptr_image;
        cv_ptr_image = cv_bridge::toCvShare(pic, "bgr8");
        cv::Mat image = cv_ptr_image->image;
        std::string path = data_path + "/picture/"+ std::to_string(primary_idx) + "_" + std::to_string(pic_idx++) + ".png";
        //ROS_INFO("Picture  Height:  %d,   Width  %d", pic->height, pic->width);
        cv::imwrite(path.c_str(), image);
        cv::waitKey(200);
    }

    void insCallBack(const data_collection::ins_msg::ConstPtr& ins) {
        if(primary_idx == 0) return;
        std::string path = data_path + "/ins/" + std::to_string(primary_idx) + "_"+std::to_string(ins_idx++) + ".txt";
        //ROS_INFO("INS  x_angle: %f,  y_angle: %f  >> '%s'", ins->x, ins->y, path.c_str());
        std::ofstream out(path, std::ios::out);
        out << "INS   x_angle:  " << ins->x << ",   y_angle:  " << ins->y;
        out.close();
    }

    void serialCallBack(const data_collection::serial_msg::ConstPtr& serial) {
        //ROS_INFO("GET SERIAL MSG: %s", serial->data);
        if(primary_idx == 0 || serial->data.size() == 0) return;
        std::string path = data_path + "/serial/" + std::to_string(primary_idx) + "_"+std::to_string(serial_idx++) + ".txt";
        //ROS_INFO("INS  x_angle: %f,  y_angle: %f  >> '%s'", ins->x, ins->y, path.c_str());
        std::ofstream out(path, std::ios::out);
        out << "Serial Message Received :   ";
        for(auto s: serial->data)
            out << s;
        out.close();
    }

    int primary_idx = 0, point16_idx = 0, pic_idx = 0, ins_idx = 0, serial_idx = 0;
    std::string data_path = "/home/jlurobot/catkin_ws/src/data_collection/data";
    ros::NodeHandle n_;
    ros::Subscriber point16_sub, point32_sub, lidar_sub, pic_sub, ins_sub, serial_sub;
};

int main(int argc, char** argv) {
    ros::init(argc, argv, "data_collection_node");
    DataCollection dataCollection;
    ros::spin();
    return 0;
}